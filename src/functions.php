<?php

namespace CreditCommons;


/**
 * Prepare a quantity for display using the configured display format.
 *
 * @staticvar type $main_format
 * @staticvar type $subdivision_format
 * @param float $raw_quant
 * @return string
 */
function displayQuant(float $raw_quant, $format) : string {
  static $main_format, $subdivision_format;
  if (!isset($main_format)) {
    $main_format = getCurrencyDisplayFormat($format);
  }
  $formatted = $main_format;
  $subdivision_format = $main_format['subdivision']??NULL;

  if (abs($raw_quant) < 1) {
    $negligible = TRUE;
    $raw_quant = $raw_quant > 0  ? 1 : -1;
  }
  if ($subdivision_format === NULL) {// If there's no subdivisions
    $formatted['main'] = $raw_quant;
    if ($pos = strrpos($formatted['main'], '.')) {
      $formatted['main'] *= pow(10, -$pos);
    }
    $formatted['subdivision'] = NULL;
  }
  else {// If there is subdivisions
    list($subs) = explode('/', $subdivision_format);
    $divisor = (int)$subs + 1; // number of subs in a single unit e.g. 100
    $formatted['main'] = floor($raw_quant / $divisor);
    $formatted['subdivision'] = $raw_quant % $divisor;
  }
  // Format / pad the subdivision
  if (!empty($subdivision_format)) {
    if ($pos = strpos($subdivision_format, '/')) {
      // Don't use / in the pad length. ⅜
      $divisor = intval(substr($subdivision_format, 0, $pos+1)) + 1;
      $pad_length = strlen(substr($subdivision_format, 0, $pos));
      if ($frac = \CCNode\decToFraction($formatted['subdivision'] / $divisor)) {
        $formatted['subdivision'] = $frac;
      }
      else {
        // no fraction found
        $formatted['subdivision'] = ':'.$formatted['subdivision'];
      }
    }
    else {
      $pad_length = strlen($subdivision_format);
      $formatted['subdivision'] = str_pad($formatted['subdivision'], $pad_length, '0', STR_PAD_LEFT);
    }
  }
  elseif ($pos = strpos($formatted['main'], '.')) {
    $dps = strlen($formatted['main']) - ($pos+1);
    $formatted['main'] = number_format((float)$formatted['main'], $dps, '.', '');
  }
  if ($raw_quant < 0) {
    array_push($formatted, '-');
  }
  $formatted = implode($formatted);
  if (isset($negligible)) {
    if ($raw_quant < 0) {
      $formatted = '&gt;'.$formatted;
    }
    else {
      $formatted = '&lt;'.$formatted;
    }
  }
  return $formatted;
}

/**
 * @return array
 * @throws \Exception
 */
function getCurrencyDisplayFormat(string $format) : array {
  // Replace the parts back into the number format.
  if ($format) {
    // Process in chunks because having a lot of trouble with long regexes
    $regexes = [
      'f1' => '^[^0]*',//everything before the first 0
      'main' => '^[0]+(\.(9+))?', //zero, followed optionally by . and some 9s
      'f2' => '^ ?<.*>[^0-9]*|[^0-9]+', // Optional text, might include html tags like img or strong
      'subdivision' => '^[0-9]+(\/[0-9]+)?', // Optional
      'f3' => '.*' // Optional
    ];
    $parts = [];
    foreach ($regexes as $key => $pattern) {
      $result = preg_match("/$pattern/", $format, $matches);
      // If there is a substring and no match
      if (is_null($matches[0]) and $key <> 'f1') {
        throw new \Exception('Badly formatted: '.$format .'--'. $format);//
      }
      $parts[$key] = $matches[0];
      // Strip the front off the string before applying the next pattern.
      $format = substr($format, strlen($matches[0]));
      if (!strlen($format)) {
        break; // we reached the end of the string.
      }
    }
    if (count($parts) == 1) {
      throw new \Exception('Format must have text and numbers.');
    }
  }
  else {
    $parts = ['main' => 0];
  }
  return $parts;
}

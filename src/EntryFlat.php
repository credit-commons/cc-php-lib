<?php

namespace CreditCommons;

use CreditCommons\CreateFromValidatedStdClassTrait;

/**
 * Simple Entry object for use in client-only application.
 *
 */
class EntryFlat {
  use CreateFromValidatedStdClassTrait;

  function __construct(
    public string $payee,
    public string $payer,
    public $quant,
    public \stdClass $metadata, // does not recognise field type: \stdClass
    public string $description = '',
    public bool $isPrimary = TRUE
  ) {
    $this->quant = (float)$this->quant;
  }

  static function create(\stdClass $data) : static {
    static::validateFields($data);
    return new static($data->payee, $data->payer, $data->quant, $data->metadata, $data->description);
  }

}

<?php

namespace CreditCommons\Exceptions;

abstract class TransactionLimitViolation extends CCViolation {

  /**
   * Constructor.
   */
  public function __construct(
    /**
     * The excess, in raw units. Sould be be multiplied by the conversion rate
     * if the error is being relayed to a trunkward node and divided by the
     * conversion rate if the error is being returned from a trunkward node.
     * This allows the error to be displayed to the client in native units.
     */
    public float $diff,
    /**
     * The account ID. Balance of trade account is represented as '*'
     */
    public string $acc
  ) {
    parent::__construct();
  }



}
